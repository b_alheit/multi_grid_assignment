//
// Created by benjamin on 2018/08/09.
//

#ifndef CDIFFUSION_CFD1DSIMULATION_H
#define CDIFFUSION_CFD1DSIMULATION_H

#include "Mesh1D.h"
#include <cmath>
#include <vector>
#include <iostream>
#include <string>
#include <dirent.h>
#include "Ab_cSolver.h"
#include <eigen3/Eigen/Dense>
#include <chrono>

using namespace Eigen;
using namespace std;

class CFD1DSimulation {
public:
    CFD1DSimulation();
    CFD1DSimulation(Mesh1D& meshIn,
                    const double& p,
                    const double& A,
                    const double& k,
                    const double& h,
                    double (*source)(double x),
                    double (*T_bound)(double x));
    void solveGaussSeidel(double e, int it);
    void solveVCycle(int coarseMeshes, int cycles, int v1, int v2);
    void printPhi();
    void writeData(string name);
    void solveGaussSeidelSolver(double e, int it, bool precondition);
    void solveGMRES_LUSGS(double e, int it, int kryVecs);
    vector<double> convertVecToVector(VectorXd in);

//    void preconditionSolver();

private:
    Mesh1D mesh;
    vector<double> phi, phiPrev, errVec, dx, resVec, solveTime;
    vector<unsigned long int> workCycles;
    double p, A, h, k;
    unsigned long int nOperations;
    vector<vector<double>*> errPtrs;
    double (*source)(double x);
    double (*TBound)(double x);
    vector<int> N;
    vector<double> vCycle(int depth, int& coarseMeshes, int& v1, int& v2, vector<double>& r);
    void interpolateAdd(vector<double>& ep, vector<double>& ec, int depth);
    void relax(vector<double>& e, vector<double>& fe, vector<double> &re, int cylces, int depth);
    void relaxPhi(int relaxations);
    void printVec(vector<double>& v);
    void storeResidual();

    Ab_cSolver solver;

    void constructSolver();

    chrono::time_point<std::chrono::high_resolution_clock> start;
    chrono::duration<double> elapsed;

};


#endif //CDIFFUSION_CFD1DSIMULATION_H
