//
// Created by benjamin on 2018/08/18.
//

#ifndef CDIFFUSION_AB_CSOLVER_H
#define CDIFFUSION_AB_CSOLVER_H

#include <iostream>
#include <vector>
#include <eigen3/Eigen/Dense>
#include <string>
#include <chrono>

using namespace std;
using namespace Eigen;


typedef double dtype;
typedef MatrixXd mat;
typedef VectorXd vec;

class Ab_cSolver {
public:
    Ab_cSolver();

    Ab_cSolver(mat& inA, vec& inB, vec& inC);

    Ab_cSolver(long unsigned int N);

    mat& accessA();

    void printA();

    void printC();

    void printB();

    void printRes();

    void solveDirectly();

    void solve1DGaussSeidel(double e, int it, bool precondition);

    void solveGMRES_LUSGS(double e, int it, int kryVecs);

//    void solveConditionedGaussSeidel(double e, int it);

    void setAValue(const int& i, const int& j, dtype value);

    void setCValue(const int& i, dtype value);

    void calcResidual();

    void storeResidualNorm();

    void preconditionDiagonal();

    void preconditionLUSGS();

    vec& getSolution();
    vector<double> getResNorms();
    vector<double> getSolveTime();
    vector<long unsigned int> getWorkCyclesVec();


private:
    mat A, D, Dinv, L, U, P, Pinv;
    vec b, c, res, deltaB;
    int solves;
    long unsigned int workCycles, n;
    vector<double> resNorms, solveTime;
    vector<long unsigned int> workCyclesVec;
    string resultsPath;

    chrono::time_point<std::chrono::high_resolution_clock> start;
    chrono::duration<double> elapsed;

    void constructD();
    void constructL();
    void constructU();
    void constructLUSGS();
};


#endif //CDIFFUSION_AB_CSOLVER_H
