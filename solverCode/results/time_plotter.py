from results import resultsReader as rr
import matplotlib.pyplot as plt


gs_results = rr.Simulation1DReader("GSTime")
v_results = rr.Simulation1DReader("VCycleTime")
LUSGS = rr.Simulation1DReader("GMRESTime")
# pre = rr.Simulation1DReader("preTest2")
# non = rr.Simulation1DReader("nonpreTest")

# print("G results")
# print(gs_results.WC)
# print(gs_results.res)
#
# print("Vresults")
# print(v_results.WC)
# print(v_results.res)

# normGRes = gs_results.res
# normVRes = v_results.res
normGRes = gs_results.res / gs_results.res[0]
normVRes = v_results.res / v_results.res[0]
normLUSGS = LUSGS.res / LUSGS.res[0]
# normPre = pre.res / pre.res[0]
# normNon = non.res / non.res[0]

print(gs_results.solveTime)
print(v_results.solveTime)
print(LUSGS.solveTime)

graph = plt

# graph.loglog(v_results.WC, normVRes, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.loglog(gs_results.WC, normGRes, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

# graph.loglog(normVRes, v_results.WC, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.loglog(normGRes, gs_results.WC, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

graph.plot(v_results.solveTime*1000, normVRes, label="V-cycle solve", marker="v", markersize=5, color="black")
graph.plot(gs_results.solveTime*1000, normGRes, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

graph.plot(LUSGS.solveTime*1000, normLUSGS, label="GMRES", marker="s", markersize=5, color="black")
# graph.plot(pre.WC, normPre, label="New preconditioned Gauss Solve", marker="^", markersize=5, color="black")
# graph.plot(non.WC, normNon, label="nonPreconditioned", marker="*", markersize=5, color="black")

# graph.plot(normVRes, v_results.WC, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.plot(normGRes, gs_results.WC, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

# graph.semilogy(v_results.solveTime, normVRes, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.semilogy(gs_results.solveTime, normGRes, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

# graph.semilogy(pre.WC, normPre, label="Preconditioned Gauss Solve", marker="s", markersize=5, color="black")
# graph.semilogy(non.WC, normNon, label="nonPreconditioned", marker="*", markersize=5, color="black")



graph.grid()
graph.legend()
graph.title("Comparison of different solving methods")
graph.ylabel("Residual")
graph.xlabel("Time (ms)")

graph.show()
