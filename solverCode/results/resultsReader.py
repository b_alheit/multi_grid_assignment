import numpy as np
import matplotlib.pyplot as plt


class Simulation1DReader:
    def __init__(self, simName):
        self.phi = np.fromfile("./" + simName + "/phi.bin", dtype=np.float, count=-1)
        self.x = np.fromfile("./" + simName + "/meshX.bin", dtype=np.float, count=-1)
        self.res = np.fromfile("./" + simName + "/res.bin", dtype=np.float, count=-1)
        self.solveTime = np.fromfile("./" + simName + "/solveTime.bin", dtype=np.float, count=-1)
        self.WC = np.fromfile("./" + simName + "/workCycles.bin", dtype=np.int64, count=-1)
        # self.mesh = Mesh1D(start, end, n_nodes, scale_factor)
        #
        # self.p = p
        # self.A = A
        # self.k = k
        # self.h = h
        # self.source = source
        # self.T_bound = temperature_boundary
        #
        # self.conductivity_matrix = np.zeros([self.mesh.n_nodes, self.mesh.n_nodes], dtype=float)
        # self.source_vector = np.ones(self.mesh.n_nodes, dtype=float)
        # self.phi_prev = np.zeros(self.mesh.n_nodes, dtype=float)
        # self.T_an = np.zeros(self.mesh.n_nodes, dtype=float)
        print("File read successfully...")

    def plot_solution(self):
        plt.plot(self.x, self.phi, marker="o")
        # plt.plot(self.x, self.T_an)
        plt.grid()
        plt.show()

