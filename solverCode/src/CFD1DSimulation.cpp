//
// Created by benjamin on 2018/08/09.
//

#include "../includes/CFD1DSimulation.h"
#include <dirent.h>
#include <sys/stat.h>
#include <fstream>
#include <vector>
#include <eigen3/Eigen/Dense>


using namespace std;
using namespace Eigen;

CFD1DSimulation::CFD1DSimulation(Mesh1D& meshIn,
                                 const double& p,
                                 const double& A,
                                 const double& k,
                                 const double& h,
                                 double (*s)(double x),
                                 double (*T)(double x)){
    this->mesh = meshIn;
    this->p = p;
    this->A = A;
    this->k = k;
    this->h = h;
    source = s;
    this -> TBound = T;
    vector<double> dummyVec(mesh.nodes());
    this->phi = dummyVec;
    this->phiPrev = dummyVec;
    this->errVec = dummyVec;
    constructSolver();

}
/*
void CFD1DSimulation::solveGaussSeidel(double e, int it){
    double resNorm = e +1;
    double sqrSum, coeff, resSqrSum;
    bool iterations;
    if(it > 0){
        cout << "Solving to " << it << " iterations" <<  endl;
        iterations = true;
    } else{
        cout << "Solving to a residual of" << e << endl;
        iterations = false;
    }
    int num = -1;
    while((!iterations && resNorm > e) || (iterations && it > num)){
        sqrSum = 0;
        for(int i = 0; i < mesh.nodes(); i++){
            if(i != 0 && i != mesh.nodes() -1){
                coeff = h*p/A - 2 * k / ((mesh[i]-mesh[i-1])*(mesh[i+1] - mesh[i]));
                phi[i] = (1/coeff) * (-source(mesh[i]) - 2 * k * (phi[i-1]/((mesh[i] - mesh[i-1]) * (mesh[i+1] - mesh[i-1])) +
                                                                 phi[i+1]/((mesh[i+1] - mesh[i]) * (mesh[i+1] - mesh[i-1]))));
            } else{
                phi[i] = TBound(mesh[i]);
            }
            errVec[i] = phi[i] - phiPrev[i];
            phiPrev[i] = phi[i];
            sqrSum += errVec[i]*errVec[i];
        }


        resNorm = pow(sqrSum/mesh.nodes(), 0.5);

        num++;
//        cout << num << endl;
    }
    cout << "Iterations: " << num << endl;

}
*/

void CFD1DSimulation::constructSolver() {
    long unsigned int nodes = mesh.nodes();

    double dx = mesh[1] - mesh[0];

    MatrixXd A(nodes, nodes);
    VectorXd b(nodes), c(nodes);
    b(0) = TBound(mesh[0]);
    c(0) = TBound(mesh[0]);
    A(0,0) = 1;

    b(nodes-1) = TBound(mesh[nodes - 1]);
    c(nodes-1) = TBound(mesh[nodes - 1]);
    A(nodes-1, nodes-1) = 1;

    for(int i = 1; i < nodes-1; i++){
        A(i, i-1) = -k/(dx*dx);
        A(i,i) = 2 *k /(dx*dx);
        A(i, i+1) = -k/(dx*dx);

        c(i) = source(mesh[i]);
    }

    solver = Ab_cSolver(A, b, c);

}

//void CFD1DSimulation::preconditionSolver() {
//    solver.preconditionDiagonal();
//}

void CFD1DSimulation::solveGaussSeidel(double e, int it){
    start = std::chrono::high_resolution_clock::now();
    double errNorm = e +1;
    double errSqrSum;
    bool iterations;
    nOperations = 0;
    double dx = mesh[1] - mesh[0];
    if(it > 0){
        cout << "Solving to " << it << " iterations" <<  endl;
        iterations = true;
    } else{
        cout << "Solving to a residual of" << e << endl;
        iterations = false;
    }
    int num = 0;
    while((!iterations && errNorm > e) || (iterations && it > num)){
        errSqrSum = 0;
        for(int i = 0; i < mesh.nodes(); i++){
            if(i != 0 && i != mesh.nodes() -1){
                phi[i] = (1.0/2) * (source(mesh[i]) * dx*dx/k + phi[i-1] + phi[i+1]);
            } else{
                phi[i] = TBound(mesh[i]);
            }
            nOperations++ ;
            errVec[i] = phi[i] - phiPrev[i];
            phiPrev[i] = phi[i];
            errSqrSum += errVec[i]*errVec[i];
        }
        storeResidual();
        errNorm = pow(errSqrSum/mesh.nodes(), 0.5);
        num++;

        printPhi();
//        cout << endl;
//        cout << resVec[num-1] << endl;
//        cout << num << endl;
    }
    cout << "Iterations: " << num << endl;

}

void CFD1DSimulation::solveGMRES_LUSGS(double e, int it, int kryVecs) {
    solver.solveGMRES_LUSGS(e, it, kryVecs);
    phi = convertVecToVector(solver.getSolution());
    workCycles = solver.getWorkCyclesVec();
    resVec = solver.getResNorms();
    solveTime = solver.getSolveTime();
}

void CFD1DSimulation::solveGaussSeidelSolver(double e, int it, bool precondition = false) {
    solver.solve1DGaussSeidel(e, it, precondition);
    phi = convertVecToVector(solver.getSolution());
    workCycles = solver.getWorkCyclesVec();
    resVec = solver.getResNorms();
    solveTime = solver.getSolveTime();
}

vector<double> CFD1DSimulation::convertVecToVector(VectorXd in) {
    unsigned long int n = in.size();
    vector<double> out(n);
    for(int i = 0; i < n; i++){
        out[i] = in(i);
    }
    return out;
}



void CFD1DSimulation::relaxPhi(int relaxations) {
    double sqrSum, coeff;
    for(int i = 0; i < relaxations; i++) {
        for (int j = 0; j < mesh.nodes(); j++) {
            if (j != 0 && j != mesh.nodes() - 1) {
                coeff = h * p / A - 2 * k / ((mesh[j] - mesh[j - 1]) * (mesh[j + 1] - mesh[j]));
                phi[j] = (1 / coeff) * (source(mesh[j]) -
                                        2 * k * (phi[j - 1] / ((mesh[j] - mesh[j - 1]) * (mesh[j + 1] - mesh[j - 1])) +
                                                 phi[j + 1] / ((mesh[j + 1] - mesh[j]) * (mesh[j + 1] - mesh[j - 1]))));

            } else {
                phi[j] = TBound(mesh[j]);
            }
            errVec[j] = phi[j] - phiPrev[j];
            phiPrev[j] = phi[j];
        }
    }

}

void CFD1DSimulation::solveVCycle(int coarseMeshes, int cycles, int v1, int v2) {
    start = std::chrono::high_resolution_clock::now();
    vector<double> sVec(mesh.nodes()*2);
    int skip;
    nOperations = 0;

    for(int i = 0; i < mesh.nodes(); i++){
        sVec[i*2] = source(mesh[i]);
    }

    for(int i = 0; i < coarseMeshes + 1; i++){
        if(i == 0) N.push_back(mesh.nodes());
        else N.push_back((N[i-1] + 1)/2);
        skip = (i +1) *i/2;
        dx.push_back(mesh[skip + 1] - mesh[0]);
    }

    for(int i = 0; i < cycles; i ++){
        phi = vCycle(0,  coarseMeshes, v1, v2, sVec);
        storeResidual();

    }
}

vector<double> CFD1DSimulation::vCycle(int depth, int& coarseMeshes, int& v1, int& v2, vector<double>& r) {

    vector<double> e(N[depth]), fe(N[depth]), re(N[depth]), eCoarseCoarse;
    if (depth == 0) e = phi;

    for(int i = 0; i < N[depth]-1; i++){
        fe[i] = r[i*2];
    }

    relax(e, fe, re, v1, depth);

    if(depth<coarseMeshes){
        eCoarseCoarse = vCycle(++depth, coarseMeshes, v1, v2, re);
        --depth;
        interpolateAdd(e, eCoarseCoarse, depth);
    }

    relax(e, fe, re, v2, depth);

    return e;
}

void CFD1DSimulation::relax(vector<double> &e, vector<double> &fe, vector<double> &re, int cylces, int depth) {
    double& dxLocal = dx[depth];
    int& n = N[depth];

    for(int i = 0; i < cylces; i++) {
        for (int j = 1; j < n - 1; j++) {
            e[j] = 0.5 * (e[j - 1] + e[j + 1] + (dxLocal * dxLocal / k) * fe[j]);
            nOperations++;
        }
        nOperations +=2;
        if(nOperations == mesh.nodes()){
            storeResidual();
        }
        if(i == cylces-1){
            for(int j = 1; j < n - 1; j++){
                re[j] = fe[j] - k * (-e[j+1] + 2 * e[j] - e[j-1]) / (dxLocal * dxLocal);

            }

        }
    }
}

void CFD1DSimulation::interpolateAdd(vector<double> &ep, vector<double> &ec, int depth) {
    int& n = N[depth];
    for(int i = 0; i <n; i++){
        if(i%2 == 0){
            ep[i] += ec[i/2];
        }else{
            ep[i] += 0.5*(ec[(i-1) / 2] + ec[(i+1)/2]);
        }
    }
}

void CFD1DSimulation::storeResidual() {
    double resSqrSum = 0;
    double dx = mesh[1] - mesh[0];
    elapsed = chrono::high_resolution_clock::now() - start;
    solveTime.push_back(elapsed.count());
    for(int j = 1; j < mesh.nodes()-1; j++){
        resSqrSum += source(mesh[j]) - (k / (dx*dx))*(2 * phi[j] - (phi[j-1] + phi[j+1]));
    }
    resVec.push_back(pow(resSqrSum,0.5));
    workCycles.push_back(nOperations);
}

void CFD1DSimulation::writeData(string name) {
    string path = "../results/" + name;
    mkdir(path.c_str(), 0777);
    ofstream phiResults, meshX, res, operations, time;
    phiResults.open(path + "/phi.bin", std::ios::binary);
    meshX.open(path + "/meshX.bin", std::ios::binary);
    res.open(path + "/res.bin", std::ios::binary);
    operations.open(path + "/workCycles.bin", std::ios::binary);
    time.open(path + "/solveTime.bin", std::ios::binary);

    for (int i=0; i < mesh.nodes(); i++){
        phiResults.write(reinterpret_cast<const char*>(&phi[i]), sizeof(double));
//        meshX.write(reinterpret_cast<const char*>(&mesh[i]), sizeof(double));
        mesh.writeMesh(meshX, i);
    }

    for(int i = 0; i < resVec.size(); i++){
        res.write(reinterpret_cast<const char*>(&resVec[i]), sizeof(double));
    }

    for(int i = 0; i < solveTime.size(); i++){
        time.write(reinterpret_cast<const char*>(&solveTime[i]), sizeof(double));
    }

    for(int i = 0; i < workCycles.size(); i++){
        operations.write(reinterpret_cast<const char*>(&workCycles[i]), sizeof(unsigned long int));
    }

    operations.close();
    res.close();
    phiResults.close();
    meshX.close();
    time.close();
}

void CFD1DSimulation::printPhi() {
    cout << "[";
    for(int i = 0; i < mesh.nodes(); i++){
        cout << phi[i];
        if(i < mesh.nodes() -1) {
            cout << "  ";
        }
    }
    cout << "]";
}

void CFD1DSimulation::printVec(vector<double> &v) {
    unsigned long int len = v.size();
    cout << "[";
    for(int i = 0; i < len; i++){
        cout << v[i];
        if(i < len -1) {
            cout << "  ";
        }
    }
    cout << "]";
}