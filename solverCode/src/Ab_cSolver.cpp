//
// Created by benjamin on 2018/08/18.
//

#include "../includes/Ab_cSolver.h"
#include <cmath>
using namespace std;
using namespace Eigen;


typedef double dtype;
typedef MatrixXd mat;
typedef VectorXd vec;

Ab_cSolver::Ab_cSolver(): solves(0), workCycles(0){;};

Ab_cSolver::Ab_cSolver(mat& inA, vec& inB, vec& inC): A(inA), b(inB), c(inC), solves(0), workCycles(0){
    n = inC.size();
    deltaB = vec(n);
    res = vec(n);
//    cout << "A" << endl << A << endl;
//    cout << "b" << endl << b << endl;
//    cout << "c" << endl << c << endl;
//    cout << "deltaB" << endl << deltaB << endl;
//    cout << "res" << endl << res << endl;
}

Ab_cSolver::Ab_cSolver(long unsigned int N) {
    A = mat::Zero(N, N);
    c = vec::Zero(N);
    deltaB = vec(N);
    res = vec(N);
    b = vec(N);
    n = N;
    solves = 0;
    workCycles = 0;
}

mat& Ab_cSolver::accessA() {return A;}

void Ab_cSolver::printA() {cout << A;}

void Ab_cSolver::printC() {cout << c;}

void Ab_cSolver::printB() {cout << b;}

void Ab_cSolver::printRes() {cout << res;}

void Ab_cSolver::setAValue(const int &i, const int &j, dtype value) {A(i,j) = value;}

void Ab_cSolver::setCValue(const int &i, dtype value) {c(i) = value;}

void Ab_cSolver::solveDirectly() {b = A.inverse() * c; solves++;}

void Ab_cSolver::solve1DGaussSeidel(double e, int it, bool precondition = false) {
    start = std::chrono::high_resolution_clock::now();
    double errNorm = e + 1;
    bool iterations;
    long int n = c.size();
    if(precondition) constructLUSGS();
    if (it > 0) {
        cout << "Solving to " << it << " iterations" << endl;
        iterations = true;
    } else {
        cout << "Solving to a residual of" << e << endl;
        iterations = false;
    }
    int num = 0;

    while ((!iterations && errNorm > e) || (iterations && it > num)) {
        if(precondition) preconditionLUSGS();
        for (int i = 1; i < n - 1; i++) {
//                b[i] = (c[i] -(A[i, i-1]*b[i-1] + A[i, i+1]*b[i+1])) / A[i,i];
            deltaB(i) = (c(i) - (A(i, i - 1) * b(i - 1) + A(i, i + 1) * b(i + 1))) / A(i, i) - b(i);
            b[i] += deltaB[i];
            workCycles++;
        }
        storeResidualNorm();
        errNorm = deltaB.norm() / pow(n, 0.5);

        num++;
    }
    cout << "Iterations: " << num << endl;
    cout << "Solved to a normalised error norm of: " << errNorm << endl;
}

void Ab_cSolver::solveGMRES_LUSGS(double e, int it, int kryVecs) {
    start = std::chrono::high_resolution_clock::now();
    chrono::time_point<std::chrono::high_resolution_clock> s1, s2, s3, s4, s5, s6, s7;
    std::chrono::duration<double> elapsed;
    s1 = std::chrono::high_resolution_clock::now();
    double errNorm = e + 1;
    double mVecR;
    bool iterations;
//    vec v0, y, w, vjT, vj, vi, a(kryVecs), bVec(kryVecs), mVec;
    vec v0, w, a(kryVecs), bVec(kryVecs);
    mat vMat(n, kryVecs), AMat(kryVecs, kryVecs);
    s2 = std::chrono::high_resolution_clock::now();
    elapsed = s2 - s1;
    cout << "Initiation time: " << elapsed.count() * 1000 << " ms" << endl;
    constructLUSGS();
    s3 = std::chrono::high_resolution_clock::now();
    elapsed = s3 - s2;
    cout << "LUSGS construction time: " << elapsed.count() * 1000 << " ms" << endl;


    if (it > 0) {
        cout << "Solving to " << it << " iterations" << endl;
        iterations = true;
    } else {
        cout << "Solving to a residual of" << e << endl;
        iterations = false;
    }
    int num = 0;

    while ((!iterations && errNorm > e) || (iterations && it > num)) {
        cout << endl << endl << "*******Iteration: " << num << " *********" << endl;

        s2 = std::chrono::high_resolution_clock::now();

        calcResidual();
        v0 = Pinv*res;
        v0.normalize();
        vMat.col(0) = v0;

        s3 = std::chrono::high_resolution_clock::now();
        elapsed = s3 - s2;
        cout << "Res calc + v0: " << elapsed.count() * 1000 << " ms" << endl;

        s5 = std::chrono::high_resolution_clock::now();
        for(int j = 0; j < kryVecs-1; j++){
            w = Pinv * (A * vMat.col(j));
            s2 = std::chrono::high_resolution_clock::now();
            elapsed = s2 - s3;
            cout << "Construct w0: " << elapsed.count() * 1000 << " ms" << endl;
            for(int i = 0; i <= j; i++){
                w -=  (vMat.col(i).dot(Pinv * (A * vMat.col(j)))) * vMat.col(i);
            }
            s3 = std::chrono::high_resolution_clock::now();
            elapsed = s3 - s2;
            cout << "Adjust w: " << elapsed.count() * 1000 << " ms" << endl;
            w.normalize();
            vMat.col(j+1) = w;
            s2 = std::chrono::high_resolution_clock::now();
            elapsed = s2 - s3;
            cout << "Normalise and store w: " << elapsed.count() * 1000 << " ms" << endl;
            s3 = std::chrono::high_resolution_clock::now();
        }

        s4 = std::chrono::high_resolution_clock::now();
        elapsed = s4 - s5;
        cout << "Find kry vecs: " << elapsed.count() * 1000 << " ms" << endl;

//
//
//        cout << "******Orthogonality check " << num << ": " << endl;
//        for(int i = 0; i < kryVecs; i++){
//            for(int j = 0; j < kryVecs; j++){
//                if(i!=j){
//                    cout << "Dot: " << vMat.col(i).dot(vMat.col(j)) << endl;
//                }
//            }
//        }
//        cout << endl << endl;


//        mVec = Pinv * A * vMat.col(0);
//        mVecR = mVec.dot(res);
//

        for(int k = 0; k < kryVecs; k++){

            bVec(k) = (A * vMat.col(k)).dot(res);
            for(int l = 0; l < kryVecs; l++){
                AMat(k,l) = (A * vMat.col(k)).dot(A * vMat.col(l));
            }
        }

        s3 = std::chrono::high_resolution_clock::now();
        elapsed = s3 - s4;
        cout << "Construct AMat: " << elapsed.count() * 1000 << " ms" << endl;
        
        a = AMat.inverse() * bVec;

        s4 = std::chrono::high_resolution_clock::now();
        elapsed = s4 - s3;
        cout << "Find a: " << elapsed.count() * 1000 << " ms" << endl;


        deltaB = vMat * a;
        deltaB(0) = 0;
        deltaB(n-1) = 0;

        b += deltaB;

        workCycles+=n;

//        cout << "b: " << endl << b << endl << endl;
//        cout << "deltaB: " << endl << deltaB << endl << endl;
//        cout << "vMat: " << endl << vMat << endl << endl;
//        cout << "a: " << endl << a << endl << endl;
//        cout << "AMat.inverse(): " << endl << AMat.inverse() << endl << endl;

        storeResidualNorm();
        errNorm = deltaB.norm() / pow(n, 0.5);

        num++;
    }
    cout << "Iterations: " << num << endl;
    cout << "Solved to a normalised error norm of: " << errNorm << endl;
}

void Ab_cSolver::preconditionDiagonal() {
//    constructD();
    calcResidual();
    deltaB = Dinv * res;
//    cout << "b Start" << endl << b << endl;
    b+= deltaB;
//    cout << "b Conditioned" << endl << b << endl;
}

void Ab_cSolver::preconditionLUSGS() {
    calcResidual();
    deltaB = Pinv * res;
    deltaB(0) = 0;
    deltaB(n-1) = 0;
    b+= deltaB;
}

void Ab_cSolver::constructD() {
    D = mat::Zero(n, n);
    Dinv = mat::Zero(n, n);
    for(unsigned long int i = 0; i < n; i++){
        D(i,i) = A(i,i);
        Dinv(i,i) = 1 / A(i,i);
    }

//    cout << "D" << endl << D << endl << endl;
//    cout << Dinv << endl << endl;
}



void Ab_cSolver::constructL() {
    L = mat::Zero(n, n);
    for(long int i = 0; i < n; i++){
        for(long int j = 0; j < i; j++){
            L(i,j) = A(i,j);
        }
    }
//    cout << "L" << endl << L << endl << endl;
}



void Ab_cSolver::constructU() {
    U = mat::Zero(n, n);
    for(long int i = 0; i < n; i++){
        for(long int j = i+1; j < n; j++){
            U(i,j) = A(i,j);
        }
    }
//    cout << "U" << endl << U << endl << endl;
}



void Ab_cSolver::constructLUSGS() {
    constructD();
    constructL();
    constructU();
    P = (L + D) * Dinv * (D + U);
    Pinv = P.inverse();
}



void Ab_cSolver::calcResidual(){res = c - (A * b);}

void Ab_cSolver::storeResidualNorm() {
    elapsed = chrono::high_resolution_clock::now() - start;
    solveTime.push_back(elapsed.count());
    workCyclesVec.push_back(workCycles);
    calcResidual();
    resNorms.push_back(res.norm());
}

vec& Ab_cSolver::getSolution() {return b;}

vector<double> Ab_cSolver::getResNorms(){return resNorms;}
vector<double> Ab_cSolver::getSolveTime(){ return solveTime;}
vector<long unsigned int> Ab_cSolver::getWorkCyclesVec(){return workCyclesVec;}
