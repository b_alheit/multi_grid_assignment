#include <iostream>
#include <vector>
#include <cmath>
#include "../includes/Mesh1D.h"
#include "../includes/CFD1DSimulation.h"
#include "../includes/Ab_cSolver.h"
#include <fstream>
#include <string>
/* To do:
 * For each iteration (V-cycle in the case of the multigrid method), calculate the Euclidean norm of the residual on all meshes
 * Document the normalized residual Euclidean norm and solution obtained on the original (finest) mesh
 (Normalization is to be done with respect to the first residual)
 * ##Plot: The solution obtained after 100 Gauss-Seidel iterations as well as that obtained after 6 multigrid cycles.
 * ##Plot: Plot the log of the normalised fine mesh residual vs. work units for both solution strategies
 *
 *
 * */
 ;

using namespace std;

double pi = 3.14159265359;
double epsilon = 0.00000000000001;

string name = "pathTest";
double start = 0.0;
double endM = 1.0;
//int nNodes = 5;
int nNodes = 129;
double scaleFactor = 1;
double k = 1.0;
double h = 0;
double TInf = 0;
double A = pi/4;
double p = pi;
double e = 0.0000000000001;
int iterations = 100;

double source(double x){
    return pow(x, 2) + 3 * x;
}

double TBound(double x){
    if(abs(x-start) < epsilon){
        return 0;
    }else if(abs(x-endM) < epsilon){
        return 0;
    }else{
        return NAN;
    }
}

int main() {

    Ab_cSolver test(2);
//    test.setAValue(0, 0, 12);
//    test.setAValue(0, 1, 8);
//    test.setAValue(1, 1, 6);
//    test.setAValue(1, 0, 5);

    test.printA();



//
//
//    Mesh1D mesh(start, endM, nNodes);
//    CFD1DSimulation sim(mesh, p, A, k, h, &source, &TBound);
//    sim.solveVCycle(5, 6, 2, 3);
////    sim.solveGaussSeidel(e, iterations);
//    sim.printPhi();
//    sim.writeData(name);
    return 0;
}