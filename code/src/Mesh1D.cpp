//
// Created by benjamin on 2018/08/09.
//

#include "../includes/Mesh1D.h"
#include <cmath>
#include <vector>
#include <iostream>
using namespace std;

Mesh1D::Mesh1D() {}

Mesh1D::Mesh1D(const double& start,const double& end,const int& nNodes,const double&scaleFactor) {
    this->start = start;
    this->end  = end;
    this->nNodes = nNodes;
    this->scaleFactor = scaleFactor;
    this->generate();
}

void Mesh1D::generate() {
    double length = end - start;
    if(abs(scaleFactor - 1.0) > 0.0000000000000001){
        double a = length * ((1-scaleFactor)/(1-(pow(scaleFactor, nNodes -1))));
        x.push_back(start);
        for(int i = 0; i < nNodes -1; i++){
            x.push_back(x[i] + a * (pow(scaleFactor, i)));
        }
    } else{
        x.push_back(start);
        double s = length / (nNodes -1);
        for(int i =0; i< nNodes-1; i++){
            x.push_back(x[i] + s);
        }
    }
}

int Mesh1D::nodes(){return nNodes;}

double Mesh1D::startPos(){ return start;}

double Mesh1D::endPos(){return end;}

double Mesh1D::scalingFactor(){ return scaleFactor;}

double Mesh1D::length() { return end - start;}

double Mesh1D::operator[](const int i) {return x[i];}

ostream& operator<<(ostream& out, const Mesh1D& mesh){
    out << "[";
    for(int i = 0; i < mesh.nNodes; i++){
        out << mesh.x[i];
        if(i < mesh.nNodes -1){
            out << "  ";
        }
    }
    out << "]";
    return out;
}

void Mesh1D::writeMesh(ofstream &writer, const int i) {
    writer.write(reinterpret_cast<const char*>(&x[i]), sizeof(double));
}