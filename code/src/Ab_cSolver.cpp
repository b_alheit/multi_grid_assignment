//
// Created by benjamin on 2018/08/18.
//

#include "../includes/Ab_cSolver.h"
#include <armadillo>


Ab_cSolver::Ab_cSolver(mat inA, colvec inC): A(inA), c(inC){};

Ab_cSolver::Ab_cSolver(long unsigned int N) {
    A = zeros<mat>(N,N);
    c = zeros<colvec>(N);
}
mat& Ab_cSolver::accessA() {return A;}

void Ab_cSolver::printA() {cout << A;}

void Ab_cSolver::setAValue(const int &i, const int &j, dtype value) {A[i,j] = value;}

void Ab_cSolver::setCValue(const int &i, dtype value) {c[i] = value;}

void Ab_cSolver::solveDirectly() {b = inv(A) * c; solves++;}
//
//void Ab_cSolver::solveDirectlyEfficient() {b = solve(A, c); solves++;}

colvec Ab_cSolver::getSolution() {return b;}