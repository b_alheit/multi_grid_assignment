from results import resultsReader as rr
import matplotlib.pyplot as plt


gs_results = rr.Simulation1DReader("Gtest")
v_results = rr.Simulation1DReader("Vtest")

# print("G results")
# print(gs_results.WC)
# print(gs_results.res)
#
# print("Vresults")
# print(v_results.WC)
# print(v_results.res)

# normGRes = gs_results.res
# normVRes = v_results.res
normGRes = gs_results.res / gs_results.res[0]
normVRes = v_results.res / gs_results.res[0]



graph = plt

# graph.loglog(v_results.WC, normVRes, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.loglog(gs_results.WC, normGRes, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

# graph.loglog(normVRes, v_results.WC, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.loglog(normGRes, gs_results.WC, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

graph.plot(v_results.WC, normVRes, label="V-cycle solve", marker="v", markersize=5, color="black")
graph.plot(gs_results.WC, normGRes, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

# graph.plot(normVRes, v_results.WC, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.plot(normGRes, gs_results.WC, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")

# graph.semilogy(v_results.WC, normVRes, label="V-cycle solve", marker="v", markersize=5, color="black")
# graph.semilogy(gs_results.WC, normGRes, label="Gauss-Seidle solve", marker="o", markersize=5, color="black")
#

graph.grid()
graph.legend()
graph.title("Comparison of different solving methods")
graph.ylabel("Residual")
graph.xlabel("Work-Cycles")

graph.show()
