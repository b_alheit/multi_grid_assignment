import numpy as np
from results import resultsReader as rr
import matplotlib.pyplot as plt


x = np.linspace(0, 1, 1000)
T_an = -((1/12) * np.power(x, 4) + 0.5 * np.power(x, 3) - (7/12) * x)

# gs_results = rr.Simulation1DReader("GS_solve")
# v_results = rr.Simulation1DReader("V_solve")

gs_results = rr.Simulation1DReader("Gtest")
v_results = rr.Simulation1DReader("Vtest")

graph = plt

graph.plot(gs_results.x, gs_results.phi, label="Gauss-Seidle solve\n(100 iterations)", marker="v", markersize=5, color="black")
graph.plot(v_results.x, v_results.phi, label="V-cycle solve\n(6 cycles, 5 coarse meshes)", marker="o", markersize=5, color="black")
graph.plot(x, T_an, label="Analytical solution", color="grey", linestyle="--", linewidth=2)

graph.grid()
graph.legend(loc=2)
graph.title("Comparison of different solving methods")
graph.ylabel("$\phi$")
graph.xlabel("$x$")

plt.show()
