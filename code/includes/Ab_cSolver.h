//
// Created by benjamin on 2018/08/18.
//

#ifndef CDIFFUSION_AB_CSOLVER_H
#define CDIFFUSION_AB_CSOLVER_H

#include <armadillo>
#include <iostream>
#include <vector>

using namespace std;
using namespace arma;

typedef double dtype;

class Ab_cSolver {
public:
    Ab_cSolver() = default;

    Ab_cSolver(mat inA, vec inC);

    Ab_cSolver(long unsigned int N);

    mat& accessA();

    void printA();
//
    void solveDirectly();
//
//    void solveDirectlyEfficient();

    void setAValue(const int& i, const int& j, dtype value);

    void setCValue(const int& i, dtype value);

    colvec getSolution();


private:
    mat A;
    colvec b, c;
    int solves = 0;
    long unsigned int workCycles;
    vector<double> resNorms, resVec;
    vector<long unsigned int> workCyclesVec;
};


#endif //CDIFFUSION_AB_CSOLVER_H
