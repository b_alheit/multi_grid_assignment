//
// Created by benjamin on 2018/08/09.
//

#ifndef CDIFFUSION_MESH1D_H
#define CDIFFUSION_MESH1D_H

#include <vector>
#include <cmath>
#include <iostream>
#include <fstream>


using namespace std;

class Mesh1D {
    friend ostream& operator<<(ostream& out, const Mesh1D& mesh);
public:
    Mesh1D();
    Mesh1D(const double& start,const double& end,const int& nNodes,const double&scaleFactor=1);
    void generate();
    int nodes();
    double startPos();
    double endPos();
    double scalingFactor();
    double length();
    double operator[](const int i);
    void writeMesh(ofstream& writer, const int i);

private:
    int nNodes;
    double start, end, scaleFactor;
    vector<double> x;


};


#endif //CDIFFUSION_MESH1D_H
